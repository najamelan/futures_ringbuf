# futures_ringbuf

## 0.2.1 - 2020-04-15

- Update to futures_codec 0.4.
- fix docs to not break on stable.
- fix CI configuration.

## 0.2.0 - 2019-01-15

- Implement tokio AsyncRead/Write. This is behind a feature flag.

## 0.1.7 - 2019-11-12

- Update to ringbuf 0.2.
- Fix a bug where a waker wasn't woken up in endpoint when the connection get's closed.

## 0.1.6 - 2019-11-12

- Update to futures 0.3.
- Test on stable as well as nightly.

## 0.1.5 - 2019-10-10

- Add Endpoint to mock full duplex connection.

## 0.1.4 - 2019-09-29

- cleanup readme
- fix wasm tests

## 0.1.3 - 2019-09-28

- update dependencies
- fix docs.rs readme

## 0.1.2 - 2019-08-15

- fix category slug that was lost in git history

## 0.1.1 - 2019-08-15

- test wasm support on CI
- update dependencies that merged bug fixes since yesterday

## 0.1.0 - 2019-08-14

- initial release.


